package ru.anenkov.tm.api.controller;

public interface IProjectController {
    void showProjects();

    void clearProjects();

    void createProject();
}
