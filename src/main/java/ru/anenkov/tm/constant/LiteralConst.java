package ru.anenkov.tm.constant;

public interface LiteralConst {

    String HELP = "Display terminal commands.";

    String VERSION = "Show version info.";

    String ABOUT = "Show developer info.";

    String EXIT = "Close application.";

    String INFO = "Display information about system.";

    String ARGUMENTS = "Show program arguments.";

    String COMMANDS = "Show program commands.";

    String TASK_CREATE = "Create new task";

    String TASK_CLEAR = "Remove all tasks";

    String TASK_LIST = "Show task list";

    String PROJECT_CREATE = "Create new project";

    String PROJECT_CLEAR = "Remove all projects";

    String PROJECT_LIST = "Show all projects";

}
