package ru.anenkov.tm.constant;

public interface TerminalConst {

    String HELP = "Help";

    String VERSION = "Version";

    String ABOUT = "About";

    String EXIT = "Exit";

    String INFO = "Info";

    String ARGUMENTS = "Arguments";

    String COMMANDS = "Commands";

    String TASK_LIST = "Task-list";

    String TASK_CLEAR = "Task-clear";

    String TASK_CREATE = "Task-create";

    String PROJECT_LIST = "Project-list";

    String PROJECT_CLEAR = "Project-clear";

    String PROJECT_CREATE = "Project-create";

}
