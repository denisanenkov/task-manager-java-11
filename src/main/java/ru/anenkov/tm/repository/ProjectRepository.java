package ru.anenkov.tm.repository;

import ru.anenkov.tm.api.repository.IProjectRepository;
import ru.anenkov.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {


    private List<Project> projects = new ArrayList<>();

    @Override
    public void add(Project project) {
        projects.add(project);
    }

    @Override
    public void remove(Project project) {
        projects.remove(project);
    }

    @Override
    public List<Project> findAll(){
        return projects;
    }

    @Override
    public void clear(){
        projects.clear();
    }

}
