package ru.anenkov.tm.repository;

import ru.anenkov.tm.api.repository.ICommandRepository;
import ru.anenkov.tm.constant.ArgumentConst;
import ru.anenkov.tm.constant.LiteralConst;
import ru.anenkov.tm.constant.TerminalConst;
import ru.anenkov.tm.model.Command;

import java.util.Arrays;

public class CommandRepository implements ICommandRepository {

    public static final Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP, LiteralConst.HELP
    );

    public static final Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT, LiteralConst.ABOUT
    );

    public static final Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION, LiteralConst.VERSION
    );

    public static final Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO, LiteralConst.INFO
    );

    public static final Command EXIT = new Command(
            TerminalConst.EXIT, null, LiteralConst.EXIT
    );

    public static final Command ARGUMENT = new Command(
            TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS, LiteralConst.ARGUMENTS
    );

    public static final Command COMMAND = new Command(
            TerminalConst.COMMANDS, ArgumentConst.COMMANDS, LiteralConst.COMMANDS
    );

    private static final Command TASK_CREATE = new Command(
            TerminalConst.TASK_CREATE, null, LiteralConst.TASK_CREATE
    );

    private static final Command TASK_CLEAR = new Command(
            TerminalConst.TASK_CLEAR, null, LiteralConst.TASK_CLEAR
    );

    private static final Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST, null, LiteralConst.TASK_LIST
    );

    private static final Command PROJECT_CREATE = new Command(
            TerminalConst.PROJECT_CREATE, null, LiteralConst.PROJECT_CREATE
    );

    private static final Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR, null, LiteralConst.PROJECT_CLEAR
    );

    private static final Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST, null, LiteralConst.PROJECT_LIST
    );

    private static final Command[] TERMINAL_COMMANDS = new Command[]{
            HELP, VERSION, ABOUT, INFO, ARGUMENT, COMMAND, TASK_CLEAR, TASK_CREATE,
            TASK_LIST, PROJECT_CLEAR, PROJECT_CREATE, PROJECT_LIST, EXIT
    };


    private final String[] COMMANDS = getCommands(TERMINAL_COMMANDS);

    private final String[] ARGS = getArgs(TERMINAL_COMMANDS);

    public String[] getCommands(Command... values) {
        if (values == null || values.length == 0) return new String[]{};
        final String[] result = new String[values.length];
        int index = 0;
        for (Command current : values) {
            final String name = current.getName();
            if (name == null || name.isEmpty()) continue;
            result[index] = name;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public String[] getArgs(Command... values) {
        if (values == null || values.length == 0) return new String[]{};
        final String[] result = new String[values.length];
        int index = 0;
        for (Command current : values) {
            final String arg = current.getArg();
            if (arg == null || arg.isEmpty()) continue;
            result[index] = arg;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public String[] getCommands() {
        return COMMANDS;
    }

    public String[] getArgs() {
        return ARGS;
    }

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
