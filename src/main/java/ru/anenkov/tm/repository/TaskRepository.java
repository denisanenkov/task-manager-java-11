package ru.anenkov.tm.repository;

import ru.anenkov.tm.api.repository.ITaskRepository;
import ru.anenkov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private List<Task> tasks = new ArrayList<>();

    @Override
    public void add(Task task) {
        tasks.add(task);
    }

    @Override
    public void remove(Task task) {
        tasks.remove(task);
    }

    @Override
    public List<Task> findAll(){
        return tasks;
    }

    @Override
    public void clear(){
        tasks.clear();
    }

}
