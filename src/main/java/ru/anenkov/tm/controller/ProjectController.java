package ru.anenkov.tm.controller;

import ru.anenkov.tm.api.controller.IProjectController;
import ru.anenkov.tm.api.service.IProjectService;
import ru.anenkov.tm.api.service.ITaskService;
import ru.anenkov.tm.model.Project;
import ru.anenkov.tm.model.Task;
import ru.anenkov.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements IProjectController {

    private IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showProjects() {
        System.out.println("[LIST PROJECTS]");
        final List<Project> projects = projectService.findAll();
        for (Project project: projects) System.out.println(project);
        System.out.println("[OK]");
    }

    @Override
    public void clearProjects() {
        System.out.println("[CLEAR PROJECTS]");
        projectService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME PROJECT: ");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION PROJECT: ");
        final String description = TerminalUtil.nextLine();
        projectService.create(name, description);
        System.out.println("[OK]");
    }

}
