package ru.anenkov.tm.bootstrap;

import ru.anenkov.tm.api.controller.ICommandController;
import ru.anenkov.tm.api.controller.IProjectController;
import ru.anenkov.tm.api.controller.ITaskController;
import ru.anenkov.tm.api.repository.ICommandRepository;
import ru.anenkov.tm.api.repository.IProjectRepository;
import ru.anenkov.tm.api.repository.ITaskRepository;
import ru.anenkov.tm.api.service.ICommandService;
import ru.anenkov.tm.api.service.IProjectService;
import ru.anenkov.tm.api.service.ITaskService;
import ru.anenkov.tm.constant.ArgumentConst;
import ru.anenkov.tm.constant.TerminalConst;
import ru.anenkov.tm.controller.CommandController;
import ru.anenkov.tm.controller.ProjectController;
import ru.anenkov.tm.controller.TaskController;
import ru.anenkov.tm.repository.CommandRepository;
import ru.anenkov.tm.repository.ProjectRepository;
import ru.anenkov.tm.repository.TaskRepository;
import ru.anenkov.tm.service.CommandService;
import ru.anenkov.tm.service.ProjectService;
import ru.anenkov.tm.service.TaskService;
import ru.anenkov.tm.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    public void run(final String[] args) {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        if (parseArgs(args)) System.exit(0);
        while (true) parseCommand(TerminalUtil.nextLine());
    }

    private void parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
        }
    }

    private void parseCommand(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case (TerminalConst.HELP):
                commandController.showHelp();
                break;
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.INFO:
                commandController.showInfo();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showTasks();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalConst.EXIT:
                commandController.exit();
                break;
        }
    }

    public boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

}

