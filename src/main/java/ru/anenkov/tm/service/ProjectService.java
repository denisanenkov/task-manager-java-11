package ru.anenkov.tm.service;

import ru.anenkov.tm.api.repository.IProjectRepository;
import ru.anenkov.tm.api.repository.ITaskRepository;
import ru.anenkov.tm.api.service.IProjectService;
import ru.anenkov.tm.model.Project;
import ru.anenkov.tm.repository.ProjectRepository;

import java.util.List;

public class ProjectService implements IProjectService {

    private IProjectRepository projectRepository = new ProjectRepository();

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(final String name) {
        final Project project = new Project();
        project.setName(name);
        projectRepository.add(project);
    }

    @Override
    public void create(final String name, final String description) {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
    }

    @Override
    public void add(final Project project) {
        if (project == null) return;
        projectRepository.add(project);
    }

    @Override
    public void remove(final Project project) {
        if (project == null) return;
        projectRepository.remove(project);
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

}
